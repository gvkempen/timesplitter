/*
 * Copyright (c) Gerard van Kempen, 2022.
 * Source available under the MIT license.
 */

package eu.vhyper.timesplitter.sys;

import java.util.Vector;

public class Triplet<T> implements Comparable<T> {

    public final int comparisonFieldIndex;
    public final Vector<T> fields;

    public Triplet(T valueA, T valueB, T valueC) {
        this(valueA, valueB, valueC, 0);
    }

    public Triplet(T valueA, T valueB, T valueC, int comparisonFieldIndex) {
        if(comparisonFieldIndex > 2) throw new RuntimeException("index out of bounds");

        this.fields = new Vector<>(3);
        this.fields.add(valueA);
        this.fields.add(valueB);
        this.fields.add(valueC);
        this.comparisonFieldIndex = comparisonFieldIndex;
    }

    @Override
    public boolean equals(Object obj) {
        final Triplet<T> refTriplet;
        try {
            refTriplet = (Triplet<T>) obj;
        } catch (ClassCastException e) {
            return false;
        }

        return (
                refTriplet.fields.get(0).equals(this.fields.get(0))
            &&  refTriplet.fields.get(1).equals(this.fields.get(1))
            &&  refTriplet.fields.get(2).equals(this.fields.get(2))
        );
    }

    @Override
    public int compareTo(Object obj) {
        final Triplet<T> refTriplet;
        try {
            refTriplet = (Triplet<T>) obj;
        } catch (ClassCastException e) {
            throw e;
        }

        int result = 0;
        if(this.equals(refTriplet)) return 0;

        return ((Comparable) (this.fields.get(this.comparisonFieldIndex))).compareTo((Comparable) (refTriplet.fields.get(refTriplet.comparisonFieldIndex)));
    }
}
