/*
 * Copyright (c) Gerard van Kempen, 2022.
 * Source available under the MIT license.
 */

package eu.vhyper.timesplitter.generators;

import eu.vhyper.timesplitter.models.*;

import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;


public class TimeGenerator {

    public static Integer minTimeUnix = 0;
    public static Integer maxTimeUnix = 24 * 60;
    public static Integer maxTimeDelta = 120;

    private TimeGenerator() {}

    public static SortedSet<TimeframeBundle> newRandomTimeframeBundles(Integer numberOfBundles, Integer sizeOfBundles) {
        Random rnd = new Random();
        SortedSet<TimeframeBundle> bundles = new TreeSet<>();
        for(Integer numIndex = 0; numIndex < numberOfBundles ; numIndex++) {
            final String identifier = numIndex.toString();
            Integer currentMinTime = minTimeUnix;

            SortedSet<Timeframe> timeFrames = new TreeSet<>();
            for(Integer sizeIndex = 0; sizeIndex < sizeOfBundles ; sizeIndex ++) {
                final Integer startTimeUnix;
                try {
                    startTimeUnix = rnd.nextInt(currentMinTime, maxTimeUnix - maxTimeDelta);
                } catch(IllegalArgumentException e) { continue; }
                final Integer stopTimeUnix = startTimeUnix + rnd.nextInt(1, maxTimeDelta);

                final Timeframe timeframe = new Timeframe(startTimeUnix, stopTimeUnix);
                timeFrames.add(timeframe);
                currentMinTime = stopTimeUnix;
            }
            final TimeframeBundle bundle = new TimeframeBundle(identifier, timeFrames);
            bundles.add(bundle);
        }
        return bundles;
    }
}
