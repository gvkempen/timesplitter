package eu.vhyper.timesplitter;

import eu.vhyper.timesplitter.generators.*;
import eu.vhyper.timesplitter.models.*;
import eu.vhyper.timesplitter.sys.Triplet;
import eu.vhyper.timesplitter.transformers.TimeTransformer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

public class Timesplitter {
    
    private static boolean print = true;

    private static String randomBundlesInputPath = "./randomBundles.j.dat";
    private static String resultBundlesOutputPath = "./outputBundles.j.dat";

    private static void dataPrint(String data) {
        if (print) System.out.println(data);
    }

    public static void main(String[] args) {
        SortedSet<TimeframeBundle> randomBundles = null;

        Boolean hasLoaded = false;
        try {
            final FileInputStream fileInputStream = new FileInputStream(randomBundlesInputPath);
            final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            final ArrayList<TimeframeBundle> randomBundlesArr = (ArrayList<TimeframeBundle>) objectInputStream.readObject();
            randomBundles = new TreeSet<>(randomBundlesArr);
            System.out.println("INFO: loaded existing test data");
            hasLoaded = true;
        } catch (Exception e) {
            System.out.println("INFO: no existing test data found, generating fresh batch");
            randomBundles = TimeGenerator.newRandomTimeframeBundles(64, 24);
        }

        if(!hasLoaded) {
            try {
                final FileOutputStream fileOutputStream = new FileOutputStream(randomBundlesInputPath);
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                final ArrayList<TimeframeBundle> randomBundlesArr = new ArrayList<>(randomBundles);
                objectOutputStream.writeObject(randomBundlesArr);
                System.out.println("INFO: finished writing test data");
            } catch (Exception e) {
                System.out.println("ERROR: could not write test data");
                System.exit(2);
            }
        }

        Map<String, Integer> identities = new HashMap<>();
        Integer identityNumber = 0;
        for(TimeframeBundle bundle : randomBundles) {
            identities.put(bundle.identifier(), identityNumber);
            identityNumber++;
        }

//        final TimeframeBundle fullIntersections = getFullIntersections(randomBundles);
        final SortedSet<LabeledTimeframe> allIntersections = getAllIntersections(randomBundles, identities);

        try {
            final FileOutputStream fileOutputStream = new FileOutputStream(resultBundlesOutputPath);
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            final ArrayList<LabeledTimeframe> intersectionsArr = new ArrayList<>(allIntersections);
            objectOutputStream.writeObject(intersectionsArr);
            System.out.println("INFO: finished writing output data");
        } catch (Exception e) {
            System.out.println("ERROR: could not write output data");
            System.exit(2);
        }

        return;
    }

    private static Integer[] getTimeBoundaries(SortedSet<Triplet<Integer>> identifiedTriplets) {
        Integer[] boundaries = new Integer[2];
        boundaries[0] = Integer.MAX_VALUE;
        boundaries[1] = 0;

        for(Triplet<Integer> triplet : identifiedTriplets) {
            if(triplet.fields.get(1) < boundaries[0]) { boundaries[0] = triplet.fields.get(1); }
            if(triplet.fields.get(2) > boundaries[1]) { boundaries[1] = triplet.fields.get(2); }
        }
        return boundaries;
    }

    private static TimeframeBundle getFullIntersections(SortedSet<TimeframeBundle> bundles) {
        if(bundles.size() == 0) { return new TimeframeBundle("INTERSECT-NULL", new TreeSet<>()); }
        SortedSet<Timeframe> intersectionFrames = bundles.first().timeframes();
        for(TimeframeBundle bundle : bundles) {
            SortedSet<Timeframe> newIntersectionFrames = new TreeSet<>();
            Timeframe previousIntersectionFrame = new Timeframe(-1, -1);
            for(Timeframe intersectionFrame : intersectionFrames) {
                for(Timeframe bundleFrame : bundle.timeframes()) {
                    Integer lowerBound = Math.max(intersectionFrame.startTimeUnix(), bundleFrame.startTimeUnix());
                    Integer upperBound = Math.min(intersectionFrame.stopTimeUnix(), bundleFrame.stopTimeUnix());
                    final Timeframe newIntersectionFrame = new Timeframe(lowerBound, upperBound);
                    if(
                            intersectionFrame.startTimeUnix() < bundleFrame.stopTimeUnix()
                        &&  bundleFrame.startTimeUnix() < intersectionFrame.stopTimeUnix()
                    ) {
                        newIntersectionFrames.add(newIntersectionFrame);
                    }
                }
            }
            intersectionFrames = newIntersectionFrames;
        }
        return new TimeframeBundle("INTERSECT", intersectionFrames);
    }

    private static SortedSet<LabeledTimeframe> getAllIntersections(SortedSet<TimeframeBundle> bundles, Map<String, Integer> identities) {
        if(bundles.size() == 0) { return new TreeSet<>(); }
        SortedSet<LabeledTimeframe> frames = new TreeSet<>();
        for(TimeframeBundle bundle : bundles) {
            final Integer bundleIdentifier = identities.get(bundle.identifier());
            final SortedSet<Integer> bundleFrameLabel = new TreeSet<>(Arrays.asList(bundleIdentifier));
            frames.addAll(TimeTransformer.timeframeBundleToLabeledFrames(bundle.timeframes(), bundleFrameLabel));
        }

        SortedSet<LabeledTimeframe> reducedFrames = new TreeSet<>();
        reducedFrames.addAll(frames);
        for (LabeledTimeframe outerFrame : frames) {
            SortedSet<LabeledTimeframe> newFrames = new TreeSet<>();
            for (LabeledTimeframe innerFrame : reducedFrames) {
                boolean hasOverlap = (
                        outerFrame.timeframe().startTimeUnix() < innerFrame.timeframe().stopTimeUnix()
                    &&  innerFrame.timeframe().startTimeUnix() < outerFrame.timeframe().stopTimeUnix()
                );

                if (hasOverlap) {
                    Integer pointA = Math.min(outerFrame.timeframe().startTimeUnix(), innerFrame.timeframe().startTimeUnix());
                    boolean pointAisInnerFrame = pointA.equals(innerFrame.timeframe().startTimeUnix());

                    Integer pointB = Math.max(outerFrame.timeframe().startTimeUnix(), innerFrame.timeframe().startTimeUnix());
                    Integer pointC = Math.min(outerFrame.timeframe().stopTimeUnix(), innerFrame.timeframe().stopTimeUnix());

                    Integer pointD = Math.max(outerFrame.timeframe().stopTimeUnix(), innerFrame.timeframe().stopTimeUnix());
                    boolean pointDisInnerFrame = pointD.equals(innerFrame.timeframe().stopTimeUnix());

                    SortedSet<Integer> labelsAB = pointAisInnerFrame ? innerFrame.labels() : outerFrame.labels();
                    LabeledTimeframe frameAB = new LabeledTimeframe(new Timeframe(pointA, pointB), labelsAB);

                    SortedSet<Integer> labelsCD = pointDisInnerFrame ? innerFrame.labels() : outerFrame.labels();
                    LabeledTimeframe frameCD = new LabeledTimeframe(new Timeframe(pointC, pointD), labelsCD);

                    if (!frameAB.timeframe().hasZeroRange() && frameAB.hasLabels()) newFrames.add(frameAB);
                    if (!frameCD.timeframe().hasZeroRange() && frameCD.hasLabels()) newFrames.add(frameCD);

                    SortedSet<Integer> labelsBC = new TreeSet<>(outerFrame.labels());
                    labelsBC.addAll(innerFrame.labels());
                    LabeledTimeframe frameBC = new LabeledTimeframe(new Timeframe(pointB, pointC), labelsBC);

                    if (!frameBC.timeframe().hasZeroRange() && frameBC.hasLabels()) newFrames.add(frameBC);
                } else {
                    newFrames.add(outerFrame);
                    newFrames.add(innerFrame);
                }
            }
            reducedFrames = newFrames;
        }

        SortedSet<LabeledTimeframe> filteredFrames = new TreeSet<>();
        Iterator<LabeledTimeframe> frameIterator = reducedFrames.iterator();
        LabeledTimeframe previousFrame = frameIterator.next();
        while(frameIterator.hasNext()) {
            LabeledTimeframe currentFrame = frameIterator.next();
            if(
                    !currentFrame.labels().containsAll(previousFrame.labels())
                ||  !currentFrame.timeframe().stopTimeUnix().equals(previousFrame.timeframe().stopTimeUnix())
            ) {
                filteredFrames.add(previousFrame);
            }
            previousFrame = currentFrame;
        }
        filteredFrames.add(previousFrame);
        return filteredFrames;
    }

}
