/*
 * Copyright (c) Gerard van Kempen, 2022.
 * Source available under the MIT license.
 */

package eu.vhyper.timesplitter.transformers;

import eu.vhyper.timesplitter.models.*;
import eu.vhyper.timesplitter.sys.Triplet;

import java.util.*;

public class TimeTransformer {
    private TimeTransformer() {}

    public record IdentifiedTriplets(
            Map<String, Integer> identities,
            SortedSet<Triplet<Integer>> triplets
    ) {}

    public static SortedSet<Triplet<Integer>> timeframeBundleToTripletSet(Integer identityNumber, SortedSet<Timeframe> bundleFrames) {
        SortedSet<Triplet<Integer>> tripletSet = new TreeSet<>();
        for(Timeframe f : bundleFrames) {
            Triplet<Integer> frameTriplet = new Triplet<>(identityNumber, f.startTimeUnix(), f.stopTimeUnix(), 1);
            tripletSet.add(frameTriplet);
        }
        return tripletSet;
    }

    public static IdentifiedTriplets timeframeBundlesToIdentifiedTriplets(SortedSet<TimeframeBundle> bundles) {
        SortedSet<Triplet<Integer>> combinedTripletSet = new TreeSet<>();
        Map<String, Integer> identities = new HashMap<>();
        Integer identityNumber = 0;

        for(TimeframeBundle bundle : bundles) {
            combinedTripletSet.addAll( timeframeBundleToTripletSet(identityNumber, bundle.timeframes()) );

            identities.put(bundle.identifier(), identityNumber);
            identityNumber++;
        }
        return new IdentifiedTriplets(identities, combinedTripletSet);
    }

    public static SortedSet<LabeledTimeframe> timeframeBundleToLabeledFrames(SortedSet<Timeframe> timeFrames, Integer label) {
        SortedSet<Integer> labels = new TreeSet<>();
        labels.add(label);
        return timeframeBundleToLabeledFrames(timeFrames, labels);
    }

    public static SortedSet<LabeledTimeframe> timeframeBundleToLabeledFrames(SortedSet<Timeframe> timeFrames, SortedSet<Integer> labels) {
        SortedSet<LabeledTimeframe> labeledFrames = new TreeSet<>();
        for(Timeframe frame : timeFrames) {
            labeledFrames.add(new LabeledTimeframe(frame, labels));
        }
        return labeledFrames;
    }
}
