/*
 * Copyright (c) Gerard van Kempen, 2022.
 * Source available under the MIT license.
 */

package eu.vhyper.timesplitter.models;

import java.io.Serializable;

public record Timeframe(
        Integer startTimeUnix,
        Integer stopTimeUnix
) implements Comparable<Timeframe>, Serializable {
    @Override
    public boolean equals(Object obj) {
        final Timeframe refFrame;
        try {
            refFrame = (Timeframe) obj;
        } catch (ClassCastException e) {
            return false;
        }
        return (
                this.startTimeUnix.equals(refFrame.startTimeUnix)
            &&  this.stopTimeUnix.equals(refFrame.stopTimeUnix)
        );
    }

    @Override
    public int compareTo(Timeframe refFrame) {
        int result = -2;
        if (equals(refFrame)) result = 0;
        else if (
                this.stopTimeUnix < refFrame.stopTimeUnix
            ||  (this.stopTimeUnix.equals(refFrame.stopTimeUnix) && this.startTimeUnix < refFrame.startTimeUnix)
        ) result = -1;
        else if (
                this.stopTimeUnix > refFrame.stopTimeUnix
            ||  (this.stopTimeUnix.equals(refFrame.stopTimeUnix) && this.startTimeUnix > refFrame.startTimeUnix)
        ) result = 1;
        return result;
    }

    public boolean hasZeroRange() {
        return this.startTimeUnix.equals(this.stopTimeUnix);
    }
}
