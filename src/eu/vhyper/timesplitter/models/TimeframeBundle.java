/*
 * Copyright (c) Gerard van Kempen, 2022.
 * Source available under the MIT license.
 */

package eu.vhyper.timesplitter.models;

import java.io.Serializable;
import java.util.SortedSet;

public record TimeframeBundle(
        String identifier,
        SortedSet<Timeframe> timeframes
) implements Comparable<TimeframeBundle>, Serializable {
    @Override
    public boolean equals(Object obj) {
        final TimeframeBundle refBundle;
        try {
            refBundle = (TimeframeBundle) obj;
        } catch (ClassCastException e) {
            return false;
        }
        return (
                this.identifier.equals(refBundle.identifier)
            &&  this.timeframes.equals(refBundle.timeframes)
        );
    }

    @Override
    public int compareTo(TimeframeBundle refBundle) {
        if (equals(refBundle)) return 0;
        return this.timeframes.first().compareTo(refBundle.timeframes.first());
    }
}
