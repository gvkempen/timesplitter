/*
 * Copyright (c) Gerard van Kempen, 2022.
 * Source available under the MIT license.
 */

package eu.vhyper.timesplitter.models;

import java.io.Serializable;
import java.util.SortedSet;

public record LabeledTimeframe(
        Timeframe timeframe,
        SortedSet<Integer> labels
) implements Comparable<LabeledTimeframe>, Serializable {
    @Override
    public boolean equals(Object obj) {
        final LabeledTimeframe refFrame;
        try {
            refFrame = (LabeledTimeframe) obj;
        } catch (ClassCastException e) {
            return false;
        }
        return( this.timeframe.equals(refFrame.timeframe) && this.labels.equals(refFrame.labels) );
    }

    @Override
    public int compareTo(LabeledTimeframe refFrame) {
        return ( this.timeframe.compareTo(refFrame.timeframe) );
    }

    public boolean hasLabels() {
        return this.labels.size() > 0;
    }
}
